from sys import argv
from time import sleep

import xlsxwriter
from splinter import Browser
from splinter import exceptions
from xlsxwriter.utility import xl_col_to_name

browser = Browser()

workbook = xlsxwriter.Workbook('output.xlsx' if len(argv) < 3 else argv[2])
worksheet = workbook.add_worksheet('AmazonCa')

header = ['Hyperlink', 'UPC', 'ASIN', 'Title', 'Price', 'Shipping Price', 'Shipping Details', 'Seller Rank', 'Prime']

row = 1
col = 0
for head in header:
    worksheet.write(0, col, head)
    col += 1

worksheet.set_column(xl_col_to_name(1) + ":" + xl_col_to_name(0), 15)
worksheet.set_column(xl_col_to_name(1) + ":" + xl_col_to_name(1), 15)
worksheet.set_column(xl_col_to_name(2) + ":" + xl_col_to_name(2), 14)
worksheet.set_column(xl_col_to_name(3) + ":" + xl_col_to_name(3), 30)
worksheet.set_column(xl_col_to_name(6) + ":" + xl_col_to_name(6), 40)
worksheet.set_column(xl_col_to_name(7) + ":" + xl_col_to_name(7), 30)

for upc in open(argv[1]):
    col = 0
    upc = upc.strip()
    browser.visit('https://www.amazon.ca/s/field-keywords=' + upc)

    retry = True
    retries = 2
    while retries > 0 and retry:
        try:
            product_url = browser.find_by_xpath('//*[@id="result_0"]/div/div/div/div[2]/div[1]/div[1]/a')['href']
            retry = False
        except exceptions.ElementDoesNotExist as ex:
            sleep(2)
            retries -= 1

    if retries == 0:
        col += 1
        worksheet.write(row, col, upc)
        col += 1
        worksheet.write(row, col, "No Product Found!")
        row += 1
        print('Not Found')
        continue

    browser.visit(product_url)

    retry = True
    all_done_except_shipping_price = False

    while retry:
        all_done_except_shipping_price = False
        product_price_shipping = ""

        asin_loc = product_url.find('dp/') + 3
        product_asin = product_url[asin_loc:asin_loc + 10]
        product_title = browser.find_by_xpath("//*[@id='productTitle']").text

        product_price_paths = [
            "//*[@id='priceblock_ourprice']",
            "//*[@id='olp_feature_div']/div/span/span",
            "//*[@id='buyNewSection']/div/div/span/span"
        ]
        product_price = "N/A"
        for product_price_path in product_price_paths:
            try:
                product_price = browser.find_by_xpath(product_price_path).text
                product_price = product_price[product_price.find('$') + 1:]
                break
            except Exception:
                continue

        product_shipping = "N/A"
        try:
            product_shipping = browser.find_by_xpath("//*[@id='merchant-info']").text
            product_prime = browser.find_by_xpath("//*[@id='merchant-info']").text
        except Exception:
            continue

        try:
            sellers_rank = browser.find_by_xpath("//*[@id='SalesRank']").text
        except Exception as ex:
            sellers_rank = ""

        product_price_shipping = "0.0"
        try:
            product_price_shipping = browser.find_by_xpath("//*[@id='soldByThirdParty']/span[2]").text
            all_done_except_shipping_price = False
        except Exception:
            all_done_except_shipping_price = True

        browse_shipping = False
        if all_done_except_shipping_price:
            try:
                retry = True
                browser.visit("https://www.amazon.ca/gp/offer-listing/{}".format(product_asin))
                browse_shipping = True
                retry = False
            except Exception:
                retry = False
                browse_shipping = False
                product_price_shipping = "0.0"

            if browse_shipping:
                try:
                    product_price_shipping = browser.find_by_xpath("//*[@id='olpOfferList']/div/div/div[2]/div[1]/p/span/span[1]").text
                    product_shipping = browser.find_by_xpath("//*[@id='olpOfferList']/div/div/div[2]/div[3]/h3/span/a").text
                    retry = False
                except Exception as ex:
                    retry = False

        if "free" in product_price_shipping.lower():
            product_price_shipping = "0.0"
        elif "$" in product_price_shipping.lower():
            product_price_shipping = product_price_shipping[product_price_shipping.find('$') + 1:].strip().split(' ')[0]

        product_asin = product_asin[product_asin.find(':') + 1:]
        if len(sellers_rank) > 0:
            sellers_rank = sellers_rank[sellers_rank.find('#'):sellers_rank.find('(') - 1]

        if "amazon" in product_prime.lower():
            product_prime = "Yes"
        else:
            product_prime = "No"

        print('Hyperlink:', product_url + '\n' +
              'UPC:', upc + '\n' +
              'ASIN:', product_asin + '\n' +
              'Title:', product_title + '\n' +
              'Price:', product_price + '\n' +
              'Shipping Price:', product_price_shipping + '\n' +
              'Shipping Details:', product_shipping + '\n' +
              'Sellers Rank:', sellers_rank + "\n" +
              'Prime:', product_prime + "\n\n")
        input_data = [product_url, upc, product_asin, product_title, product_price,
                      product_price_shipping, product_shipping, sellers_rank, product_prime]
        for data in input_data:
            worksheet.write(row, col, data)
            col += 1
        row += 1
        retry = False

workbook.close()
browser.quit()
